import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MovieTest {
    Movie movie;
    String title;
    int price;

    @BeforeEach
    public void setUp() {
        title = "Fowler";
        price = 1;
        movie = new Movie(title, price);
    }

    @Test
    void getPriceCode() {
        assertEquals(price, movie.getPriceCode());
    }

    @Test
    void setPriceCode() {
        movie.setPriceCode(3);
        assertEquals(3, movie.getPriceCode());
    }

    @Test
    void getTitle() {
        assertEquals(title, movie.getTitle());
    }
}