import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RentalTest {
    private Movie movie;
    private Rental rental;
    private int daysRented = 24;

    @BeforeEach
    public void setUp() {
        movie = new Movie("Fowler", 1);
        rental = new Rental(movie, daysRented);
    }

    @Test
    void getDaysRented() {
        assertEquals(daysRented, rental.getDaysRented());
    }

    @Test
    void getMovie() {
        assertEquals(movie, rental.getMovie());
    }
}